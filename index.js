// biến dùng chung cho tất cả function
var numberArray = [];
var newIntegerArray = [];

// thêm số vào dãy
function themSoVaoDay() {
  var numberInput = document.querySelector("#numberInput").value * 1;
  numberArray.push(numberInput);
  document.querySelector("#numberArray").innerHTML = numberArray;
}

// Bài 1
function ketQuaB1(){
  var tongsoDuong = 0;

  for (var index = 0; index < numberArray.length; index++) {
    var currentNumber = numberArray[index];

    if (currentNumber > 0) {
      tongsoDuong += currentNumber;
    }
  }
  document.querySelector('#showKetQuaB1').innerHTML = tongsoDuong;
}

// Bài 2
function ketQuaB2(){
  var demSoDuong = 0;

  for (var index = 0; index < numberArray.length; index++) {
    var currentNumber = numberArray[index];

    if (currentNumber > 0) {
      demSoDuong++;
    }
  }
  document.querySelector('#showKetQuaB2').innerHTML = demSoDuong;
}

// Bài 3
function ketQuaB3(){
  var soNhoNhat = numberArray[0];

  for (var index = 0; index < numberArray.length; index++) {
    var currentNumber = numberArray[index];

    if (soNhoNhat > currentNumber) {
      soNhoNhat = currentNumber;
    }
  }
  document.querySelector('#showKetQuaB3').innerHTML = soNhoNhat;
}

// Bài 4
function ketQuaB4(){
  var positiveArray = [];
  
  // lọc số dương ra 1 mảng mới
  for (var i = 0; i < numberArray.length; i++) {
    var currentNumber = numberArray[i];
    if (currentNumber > 0){
      positiveArray.push(currentNumber)
    }
  }

  // so sánh vòng lặp tìm ra số nhỏ nhất trong mảng mới
  var soDuongNhoNhat = positiveArray[0];
  for (var j = 0; j < positiveArray.length; j++) {
    var currentPosNumber = positiveArray[j];
    if (currentPosNumber < soDuongNhoNhat){
      soDuongNhoNhat = currentPosNumber;
    }
  }
  document.querySelector('#showKetQuaB4').innerHTML = soDuongNhoNhat;
}

// Bài 5
function ketQuaB5(){
  var soChanCuoiCung = 0;
  var n = numberArray.length;
  for (var i = n - 1; i >= 0; i--) {
    if (numberArray[i] % 2 == 0) {
      soChanCuoiCung = numberArray[i];
      break;
    }
    else{
      soChanCuoiCung = -1;
    }
  }
  document.querySelector('#showKetQuaB5').innerHTML = soChanCuoiCung;
}

// Bài 6
function ketQuaB6(){
  var viTri1 = document.querySelector('#viTri1').value * 1;
  document.querySelector('#giaTriViTriMot').value = numberArray[viTri1];
  var viTri2 = document.querySelector('#viTri2').value * 1;
  document.querySelector('#giaTriViTriHai').value = numberArray[viTri2];
  var tempValue = numberArray[viTri1];
  
  numberArray[viTri1] = numberArray[viTri2];
  numberArray[viTri2] = tempValue;
  
  document.querySelector('#showKetQuaB6').innerHTML = numberArray;
}

// Bai 7
function ketQuaB7(){
  for (var index = 0; index < numberArray.length - 1; index++){
    var indexMin = index;
    for (var indexSub = index + 1; indexSub < numberArray.length; indexSub++){
      if (numberArray[indexSub] < numberArray[indexMin]){
        indexMin = indexSub;
      }
    }
    var indexInc = numberArray[index];
    numberArray[index] = numberArray[indexMin];
    numberArray[indexMin] = indexInc;
  }
  document.querySelector('#showKetQuaB7').innerHTML = numberArray;
}

// Bài 8
function checkPrimeNumber(number){
  var checkPrimeNumber = true;
  for (var i = 2; i <= Math.sqrt(number); i++){
    if (number % i == 0){
      checkPrimeNumber = false;
      break;
    }
  }
  return checkPrimeNumber;
}
function ketQuaB8(){
  for (var index = 0; index < numberArray.length; index++){
    var currentNumber = numberArray[index];
    var checkSNT = checkPrimeNumber(currentNumber);
    if (currentNumber >=2 && checkSNT == true){
      break;
    }else{
      currentNumber = -1;
    }
  }
  document.querySelector('#showKetQuaB8').innerHTML = currentNumber;
}

// Bài 9
function themArrayB9(){
  var newNumberInput = document.querySelector('#newNumberInput').value * 1;
  newIntegerArray.push(newNumberInput);
  document.querySelector('#newIntegerArray').innerHTML = newIntegerArray;
}
function ketQuaB9(){
  var integerNumber = 0;
  for (var index = 0; index < newIntegerArray.length; index++){
    var currentNumberNew = newIntegerArray[index];
      if ((Number.isInteger(currentNumberNew)) == true){
        integerNumber++;
      }
  }
  document.querySelector('#showKetQuaB9').innerHTML = integerNumber;
}

// Bài 10
function ketQua10(){
  var soLuongSoAm = 0;
  var soLuongSoDuong = 0;
  for (var index = 0; index < numberArray.length; index++){
    var currentNumber = numberArray[index];
    if (currentNumber > 0){
      soLuongSoDuong++;
    }else if (currentNumber < 0){
      soLuongSoAm++;
    }
  }
  if (soLuongSoDuong > soLuongSoAm){
    document.querySelector('#showKetQuaB10').innerHTML = `Số dương > Số âm`;
  }else if (soLuongSoAm > soLuongSoDuong){
    document.querySelector('#showKetQuaB10').innerHTML = `Số dương < Số âm`;
  }
  else{
    document.querySelector('#showKetQuaB10').innerHTML = `Số dương = Số âm`;
  }
}